import ReactDOM from 'react-dom';
import React from 'react';
import { useStrict } from 'mobx';
import { Provider } from 'mobx-react';

import App from './components/App.jsx';

import contactStore from './stores/contactStore.jsx';
import UIStore from './stores/uiStore.jsx';

const stores = {
    contactStore,
    UIStore,
};

useStrict(true);

ReactDOM.render((
    <Provider {...stores}>
        <App />
    </Provider>
), document.getElementById('root'));