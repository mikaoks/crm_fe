import { observable } from 'mobx';
import uuidv1 from 'uuid';

export default class Contact {

    @observable id;
    @observable yritys;
    @observable henkilo;
    @observable puhelin;
    @observable email;
    @observable kuvaus;

    constructor(yritys, henkilo, puhelin, email, kuvaus) {
        this.id = uuidv1();
        this.yritys = yritys;
        this.henkilo = henkilo;
        this.puhelin = puhelin;
        this.email = email;
        this.kuvaus = kuvaus;
    }

}