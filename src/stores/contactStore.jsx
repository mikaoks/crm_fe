import { observable, computed, action } from "mobx";

import Contact from "../models/Contact";


class ContactStore {

    @observable contacts = [];

    @action
    addContact(yritys, henkilo, puhelin, email, kuvaus) {
        this.contacts.push(new Contact(yritys, henkilo, puhelin, email, kuvaus));
    }

}
export default new ContactStore();