import { observable,  action } from "mobx";


class UIStore {

    @observable menuItemLeftValue;
    @observable contactsActive;
    @observable newContactOpen;

    constructor() {
        this.menuItemLeftValue = 'Kontaktit';
        this.contactsActive  = true;
        this.newContactOpen = false;
    }

    @action
    changeMenuItemLeftValue(value) {
        this.menuItemLeftValue = value;
        value === 'Kontaktit' ? this.contactsActive = true : this.contactsActive = false;
    }

    @action
    handleNewContactClose() {
        this.newContactOpen = false;
    }

    @action
    handleNewContactOpen() {
        this.newContactOpen = true;
    }

}
export default new UIStore();