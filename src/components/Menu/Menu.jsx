import React from 'react';
import AppBar from 'material-ui/AppBar';
import { inject, observer } from 'mobx-react';

import MenuItemLeft from "./MenuItemLeft";


@inject('contactStore', 'UIStore')
@observer
export default class Menu extends React.Component {

    render() {
        return (
            <div>
               <AppBar
                   title={this.props.UIStore.menuItemLeftValue}
                   iconElementLeft={<MenuItemLeft />}
               />
            </div>
        );
    }

}
