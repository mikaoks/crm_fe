import React from 'react';
import { inject, observer } from 'mobx-react';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import MenuItem from 'material-ui/MenuItem';


@inject('UIStore')
@observer
export default class MenuItemLeft extends React.Component {

    handleChange = (event, value) => {
        this.props.UIStore.changeMenuItemLeftValue(value);
    };

    render() {
        return (
            <div>
                <IconMenu
                    iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                    onChange={this.handleChange}
                    value={this.props.UIStore.menuItemLeftValue}
                >
                    <MenuItem value={'Kontaktit'} primaryText={'Kontaktit'}/>
                    <MenuItem value={'Liidit'} primaryText={'Liidit'}/>
                    <MenuItem value={'Soittolistat'} primaryText={'Soittolistat'}/>
                    <MenuItem value={'Passiivit'} primaryText={'Passiivit'}/>
                </IconMenu>
            </div>
        );
    }

}
