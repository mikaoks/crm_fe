import React from 'react';

import ActionBarContacts from './ActionBarContacts';
import ContactTable from './ContactTable';

export default class Contacts extends React.Component {

    render () {
        return (
            <div>
                <ActionBarContacts />
                <ContactTable/>
            </div>
        );

    }
}