import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import { inject, observer } from 'mobx-react';

@inject('contactStore')
@observer
export default class ContactTable extends React.Component {

    render() {
        return (
            <div>
                <Table>
                    <TableHeader>
                        <TableRow>
                            <TableHeaderColumn>Yritys</TableHeaderColumn>
                            <TableHeaderColumn>Henkilö</TableHeaderColumn>
                            <TableHeaderColumn>Puhelinnumero</TableHeaderColumn>
                            <TableHeaderColumn>Sähköposti</TableHeaderColumn>
                            <TableHeaderColumn>Kuvaus</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        {this.props.contactStore.contacts.map( contact => (
                            <TableRow key={contact.id}>
                                <TableRowColumn>{contact.yritys}</TableRowColumn>
                                <TableRowColumn>{contact.henkilo}</TableRowColumn>
                                <TableRowColumn>{contact.puhelin}</TableRowColumn>
                                <TableRowColumn>{contact.email}</TableRowColumn>
                                <TableRowColumn>{contact.kuvaus}</TableRowColumn>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
        );
    }
}
