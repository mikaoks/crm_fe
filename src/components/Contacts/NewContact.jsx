import React from 'react';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import { inject, observer } from 'mobx-react';

@inject('UIStore','contactStore')
@observer
export default class NewContact extends React.Component {

    handleClose = () => {
        this.props.UIStore.handleNewContactClose();
    }

    handleSubmit = () => {
        this.props.contactStore.addContact(
            this.refs.yritys.getValue(),
            this.refs.henkilo.getValue(),
            this.refs.puhelin.getValue(),
            this.refs.email.getValue(),
            this.refs.kuvaus.getValue()
        );
        this.props.UIStore.handleNewContactClose();
    }

    render() {

        const textStyles = {
            margin: 5,
        };

        const actions = [
            <FlatButton
                type="text"
                label="Peruuta"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                type="submit"
                label="OK"
                primary={true}
                keyboardFocused={true}
                onClick={this.handleSubmit}
            />,
        ];

        return (
            <Dialog
                title={"Lisää uusi kontakti"}
                open={this.props.UIStore.newContactOpen}
                actions={actions}
                onRequestClose={this.handleClose}
            >
                <div>
                    <TextField
                        id={"yritys"}
                        ref="yritys"
                        floatingLabelText={"Yritys:"}
                        style={textStyles}
                    />
                    <TextField
                        id={"henkilo"}
                        ref="henkilo"
                        floatingLabelText={"Henkilö:"}
                        style={textStyles}
                    />
                    <br/>
                    <TextField
                        id={"puhelin"}
                        ref="puhelin"
                        floatingLabelText={"Puhelin:"}
                        style={textStyles}
                    />
                    <TextField
                        id={"email"}
                        ref="email"
                        floatingLabelText={"Sähköposti:"}
                        style={textStyles}
                    />
                    <br/>
                    <TextField
                        id={"kuvaus"}
                        ref="kuvaus"
                        floatingLabelText={"Kuvaus:"}
                        style={textStyles}
                        multiLine={true}
                        rows={3}
                    />
                </div>
            </Dialog>
        )
    };

}


