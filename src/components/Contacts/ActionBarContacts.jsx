import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import { inject, observer } from 'mobx-react';

import NewContact from "./NewContact";

@inject('contactStore', 'UIStore')
@observer
export default class ActionBarContacts extends React.Component {

    handleNewContact = () => {
        this.props.UIStore.handleNewContactOpen();
    };

    render() {
        return (
            <div>
                <Toolbar>
                    <ToolbarGroup>
                        <RaisedButton onClick={this.handleNewContact} label={"Uusi kontakti"}/>
                        <NewContact/>
                    </ToolbarGroup>
                </Toolbar>
            </div>
        );
    }

}
