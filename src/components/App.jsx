'use strict';
import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { observer } from 'mobx-react';
import { inject } from 'mobx-react';

import Menu from './Menu/Menu';
import Contacts from './Contacts/Contacts';

@inject('UIStore', 'contactStore')
@observer
export default class App extends React.Component {

    componentDidMount() {
        this.props.contactStore.addContact(
            "Nokia",
            "Matti",
            "050 387 6543",
            "matti@nokia.com",
            "Matilla olisi projektia tarjolla"
        )
        this.props.contactStore.addContact(
            "Länsimetro",
            "Seppo",
            "09 5897 7635",
            "seppo@lansimetro.fi",
            "Länsimetrolla ongelmia"
        )
    }

    render() {

        let content = null;
        if (this.props.UIStore.contactsActive) {
            content = <Contacts />
        } else {
            content = <h1>Toiminnallisuutta ei vielä tehty!</h1>
        }

        return (
            <MuiThemeProvider>
                <div>
                    <Menu />
                    {content}
                </div>
            </MuiThemeProvider>

        );
    }
}
